1. open het bestand [public/index.html](../public/index.html), klik op Edit (rechtsboven), hierdoor ontstaat een nieuwe branche en pas vervolgens de tekst aan op basis van de instructies in je Team-folder.


2. vul de commit message in, commit op je eigen branche door op commit changes te klikken, merge request naar Master, docent accepteert en automatisch volgen de Build en Deploy stappen, ga naar de webpagina om het resultaat te zien