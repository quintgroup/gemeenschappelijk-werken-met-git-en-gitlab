# Team 3 
Er zijn meerdere klachten over de web-site [quintlabs.nl](http://quintlabs.nl), zo lijkt de tekst van de pagina wat gedateerd, geef in het issue aan welk jaartal je in ieder geval zou willen aanpassen:
1. Open een issue, en geef aan welke wijziging jullie team voorstelt.
2. Bij assignee vul je team_3 in, de rest zoals milestone kan je leeg laten..
3. Kijk even rond hoe je issue in de lijst staat (List in Issue menu aan linker zijde), kijk rond hoe de boards er uitzien en kijk bij de milestones welke al gedefinieerd zijn..
4. In de tussentijd krijg je antwoord op je issue en kan verder met opdracht 1.2