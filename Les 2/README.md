# Les 2 Provisioning, de bodem...
Voor provisioning maken we gebruik van Google Cloud Compute Egine (GCE) en specifiek werken we met de Google Cloud Console. Deze omgeving kan gebruikt worden om op basis van immutable infrastructuur principes omgevingen neer te zetten en conform de functionele specificaties beschikbaar te houden.


Iedereen heeft vooraf toegang gekregen tot Google Cloud, als je met je Google Quintgroup account in de browser sessie op je laptop zit, dan kan je direct aan de slag. 

## Leerdoelen
1. In deze oefningen leer je de console en deployment tooling in te zetten om virtuele machines uit te rollen,
2. waarbij je een cloud virtuele machine (vm) aanzet door een configuratie bestand aan te passen
3. en je daarna de omgeving weer opruimt op basis van je eerdere deployment.

Met de onderstaande button open je de tutorial omgeving in Google Cloud. 

Bij de eerste keer dat je op de Google Cloud omgeving komt, zal dit veel pop-ups genereren, lees kort de meldingen door en vink waar nodig voor akkoord. Ook komt er een melding voorbij of je een Git repositry wil synchroniseren, klik bij deze vraag op proceed of akkoord. Mocht e.e.a. niet het gewenste resultaat opleveren, sluit het tabblad en druk nogmaals op de knop hieronder.

**Mocht je extra beveiliging in je browser aan hebben staan tegen cookies etc. zet deze dan gedurende de tutorial uit.**

[![Open this project in Cloud
Shell](http://gstatic.com/cloudssh/images/open-btn.png)](https://console.cloud.google.com/cloudshell/open?git_repo=https://github.com/quintest/cloudshell-tutorials-summerschool.git&page=editor&tutorial=provisioning/tutorial.md)

